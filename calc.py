def cinf(a):
    return a * 9 / 5 + 32

def cink(a):
    return a + 273.15

def finc(a):
    return (a - 32) * 5 / 9

def fink(a):
    return finc(a) + 273.15

def kinc(a):
    return a - 273.15


def kinf(a):
    return cinf(kinc(a))


def temp_calc(t, first_scale, second_scale):
    first_scale = first_scale.upper()
    second_scale = second_scale.upper()
    file = open("test.txt", "a")
    if (first_scale != "C" and first_scale != "F" and first_scale != "K") or (second_scale != "C" and second_scale != "F"
                                                                              and second_scale != "K"):
        file.write("Ошибка шкалы\n")
        return "Ошибка шкалы"
    if first_scale == "C":
        if second_scale == "F":
            temp = cinf(t)
        if second_scale == "K":
            temp = cink(t)
    else:
        if first_scale == "F":
            if second_scale == "C":
                temp = finc(t)
            if second_scale == "K":
                temp = fink(t)
        if first_scale == "K":
            if second_scale == "C":
                temp = kinc(t)
            if second_scale == "F":
                temp = kinf(t)
    if first_scale == second_scale:
        temp = t
    file.write(str(t) + " " + first_scale + " = " + str(temp) + " " + second_scale + "\n")
    return temp
