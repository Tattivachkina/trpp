from calc import temp_calc


def test_1():
    assert temp_calc(26, "F", "K") == 269.81666666666666

def test_2():
    assert temp_calc(293, "K", "C") == 19.850000000000023


def test_3():
    assert temp_calc(20, "C", "F") == 68


def test_4():
    assert temp_calc(98, "F", "C") == 36.666666666666664


# test_1()
# test_2()
# test_3()
# test_4()
